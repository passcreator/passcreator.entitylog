<?php

namespace Passcreator\EntityLog\Subscriber;

/*
 * This script belongs to the FLOW3 package "SporerWebservices.PassbookPasses".*
 * *
 */
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Neos\Flow\Annotations as Flow;

/**
 *
 */
class LoggingSubscriber implements EventSubscriber
{

    const STATUS_NEW = 1;
    const STATUS_UPDATE = 2;
    const STATUS_DELETE = 3;

    /**
     * @Flow\InjectConfiguration(path="auditedEntities")
     * @var string
     */
    protected $auditedEntities;

    /**
     * @Flow\InjectConfiguration(path="logger")
     * @var string
     */
    protected $logger;

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
            'postRemove',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->logChange($args, LoggingSubscriber::STATUS_NEW);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->logChange($args, LoggingSubscriber::STATUS_UPDATE);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postRemove(LifecycleEventArgs $args)
    {
        $this->logChange($args, LoggingSubscriber::STATUS_DELETE);
    }

    /**
     * @param LifecycleEventArgs $args
     * @param int $status
     */
    protected function logChange(LifecycleEventArgs $args, $status)
    {
        if($this->isEntityAudited(get_class($args->getEntity()))) {
            $loggerClassName = $this->getLoggerClassName();
            $logger = new $loggerClassName();
            $logger->logChange($args, $status);
        }
    }

    /**
     * @param string $entityClass
     * @return bool
     */
    protected function isEntityAudited($entityClass) {
        if($this->auditedEntities === NULL) {
            return true;
        }

        if(array_key_exists($entityClass, $this->auditedEntities)) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    protected function getLoggerClassName() {
        if(isset($this->logger['className'])) {
            return $this->logger['className'];
        }

        return 'Passcreator\EntityLog\Logger\FileLogger';
    }
}