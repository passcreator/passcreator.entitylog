<?php

namespace Passcreator\EntityLog\Logger;

/**
 * Interface FileLoggerInterface
 * @package Passcreator\EntityLog\Logger
 */
interface EntityLoggerInterface extends \Psr\Log\LoggerInterface
{

}
