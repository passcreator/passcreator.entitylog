<?php

namespace Passcreator\EntityLog\Logger;

use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 *
 */
interface LoggerInterface
{

    /**
     * @param LifecycleEventArgs $args
     * @param int $status
     * @return void
     */
    public function logChange(LifecycleEventArgs $args, $status);

}