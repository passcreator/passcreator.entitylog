<?php

namespace Passcreator\EntityLog\Logger;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use Passcreator\EntityLog\Subscriber\LoggingSubscriber;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Message;
use Neos\Flow\Persistence\PersistenceManagerInterface;

/**
 * Logs the given change to the default File logger
 */
class FileLogger implements LoggerInterface
{

    /**
     * @Flow\InjectConfiguration(path="auditedEntities")
     * @var string
     */
    protected $auditedEntities;

    /**
     * @var PersistenceManagerInterface
     * @Flow\Inject
     */
    protected $persistenceManager;

    /**
     * @var EntityLoggerInterface
     * @Flow\Inject
     */
    protected $logger;

    /**
     * @param LifecycleEventArgs $args
     * @param int $status
     * @return void
     */
    public function logChange(LifecycleEventArgs $args, $status)
    {
        switch ($status) {
            case LoggingSubscriber::STATUS_NEW:
                $this->logCreate($args);
                break;
            case LoggingSubscriber::STATUS_UPDATE:
                $this->logUpdate($args);
                break;
            case LoggingSubscriber::STATUS_DELETE:
                $this->logDelete($args);
                break;
        }
    }

    /**
     * Logs the persistence identifier of a new object
     * @param LifecycleEventArgs $args
     */
    protected function logCreate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->logger->info('An object of ' . get_class($entity) . ' with Persistence Identifier ' . $this->persistenceManager->getIdentifierByObject($entity) . ' has been created.');
    }

    /**
     * Uses the unit of work to log the changes of the updated object
     * @param LifecycleEventArgs $args
     */
    protected function logUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $entityManager = $args->getEntityManager();
        $unitOfWork = $entityManager->getUnitOfWork();
        $unitOfWork->computeChangeSets();
        $changeSet = $unitOfWork->getEntityChangeSet($entity);

        $changeSet = $this->removeIgnoredPropertiesFromChangeSet($changeSet, $entity);
        $changes = $this->buildChangesString($changeSet);

        $this->logger->info('An object of ' . get_class($entity) . ' has been updated. The following data changed: ' . $changes);
    }

    /**
     * Logs the persistence identifier of a deleted object
     * @param LifecycleEventArgs $args
     */
    protected function logDelete(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->logger->info('An object of ' . get_class($entity) . ' with Persistence Identifier ' . $this->persistenceManager->getIdentifierByObject($entity) . ' has been deleted.');
    }

    /**
     * Removes properties from the change set that should be ignored
     * @param array $changeSet
     * @param $entity
     * @return array
     */
    protected function removeIgnoredPropertiesFromChangeSet($changeSet, $entity)
    {
        // check if the current entity has ignored properties
        if (isset($this->auditedEntities[get_class($entity)])) {
            $ignoredProperties = $this->auditedEntities[get_class($entity)];
        } else {
            return $changeSet;
        }

        // check which properties are ignored and remove them from the array
        foreach ($ignoredProperties as $ignoredProperty) {
            if (isset($changeSet[$ignoredProperty])) {
                unset($changeSet[$ignoredProperty]);
            }
        }

        return $changeSet;
    }

    /**
     * @param array $changeSet
     * @return string
     */
    protected function buildChangesString($changeSet)
    {
        $changesString = '';

        foreach ($changeSet as $changedProperty => $changes) {
            if (is_string($changes[1])) {
                $changesString = $changesString . 'PROPERTY: ' . $changedProperty . ' - Old value: ' . $changes[0] . ' - New value: ' . $changes[1] . '       ';
            }

            if ($changes[1] instanceof \DateTime && $changes[0] instanceof \DateTime) {
                $changesString = $changesString . 'PROPERTY: ' . $changedProperty . ' - Old value: ' . $changes[0]->format('Y-m-d H:i:s') . ' - New value: ' . $changes[1]->format('Y-m-d H:i:s') . '       ';
            }

            if (is_array($changes[1])) {
                $changesString = $changesString . 'PROPERTY: ' . $changedProperty . ' - Old value: ' . \Neos\Flow\var_dump($changes[0],
                        null, true, true) . ' - New value: ' . \Neos\Flow\var_dump($changes[1], null, true,
                        true) . '       ';
            }
        }

        return $changesString;
    }

}
