[![MIT license](http://img.shields.io/badge/license-MIT-brightgreen.svg)](http://opensource.org/licenses/MIT)

# Entity Log Package for Flow Framework

This [Flow](https://flow.neos.io/) package allows you to log CUD (create, update, delete) changes in your own Flow packages.
To do this a Doctrine Event Subscriber is used.
By default a FileLogger is used that writes all changes to an EntityLog.log file in Data/Logs.
You can change this behaviour by implementing your own Logger.

## Installation

To install the package just do a

```bash
$ composer require passcreator/entitylog
```

Afterwards changes to doctrine entities should be logged to Data/Logs/EntityLog.log


## Configure Logging

By default all changes to all entities are being logged unless you explicitly specify entities for logging.

### Log all changes to an entity

To log all changes of an entity, add the following to your Settings.yaml
As soon as one or more entities are specified for logging int your Settings only these entities will be logged.

```yaml
Passcreator:
  EntityLog:
    auditedEntities:
      'My\Package\Example\Entity':
```

### Ignore certain properties

If you need to exclude certain properties from logging, you can do so by specifying them in the Settings.yaml.
This is usefule e.g. if you don't want to have sensitive data in your logfiles.

Example - exclude properties:

```yaml
Passcreator:
  EntityLog:
    auditedEntities:
      'My\Package\Example\Entity':
        - 'ignoredProperty1'
        - 'ignoredProperty2'
```


## Use your own logger

The package allows you to create your own Logger to implement other logging features than the default behaviour.
E.g. you could log changes using a Doctrine entity.

In general it works this way:
- create a class that implements the LoggerInterface of the Passcreator.EntityLog package
- specify that class in your Settings file.
- your own class is now being used for logging.

Have a look at the default file logger that is used by default in this package.

Specifying your own Logger:
```yaml
Passcreator:
  EntityLog:
    logger:
      className: 'My\Package\Logger\CustomLogger'
```